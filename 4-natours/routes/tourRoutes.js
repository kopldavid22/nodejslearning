const express = require('express');
const tourController = require('../controllers/tourController');

const router = express.Router();

//middleware which is called just when params is in route (just tour route in this case)
router.param('id', tourController.checkID);

//REQUEST RESPONSE CYCLE
//ROUTERS
router
  .route('/')
  .get(tourController.getAllTours)
  .post(tourController.checkBody, tourController.createTour);
router
  .route('/:id')
  .get(tourController.getTour)
  .patch(tourController.updateTour)
  .delete(tourController.deleteTour);

module.exports = router;
